package com.example.customer

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.example.base.BaseFragment
import com.example.base.initViewModel
import com.example.democallapi.R
import com.example.democallapi.databinding.FragmentCustomerBinding


class FragmentCustomer : BaseFragment<CustomerViewModel, FragmentCustomerBinding>() {
    override fun onCreateViewModel(): CustomerViewModel= initViewModel()

    override fun layoutId(): Int = R.layout.fragment_customer

    override fun initView() {

    }

}