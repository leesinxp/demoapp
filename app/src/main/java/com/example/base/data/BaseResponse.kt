package com.example.base.data

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

class BaseResponse<T> {
    @SerializedName("status")
    @Expose
    var status : Int?= null
    @SerializedName("message")
    @Expose
    var message : String?= null
    @SerializedName("data")
    @Expose
    var data : T?= null
    override fun toString(): String {
        return "BaseResponse{" +
                "status=" + status +
                ", message='" + message + '\'' +
                ", data=" + data +
                '}'
    }
}