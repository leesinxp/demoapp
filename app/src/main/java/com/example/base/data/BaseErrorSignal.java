package com.example.base.data;

public class BaseErrorSignal {
    public BaseErrorSignal(int errorCode, String errorMessage) {
        this.errorCode = errorCode;
        this.errorMessage = errorMessage;
    }

    private int errorCode;
    private String errorMessage;

    public static final int ERROR_HAS_MESSAGE = 0;
    public static final int ERROR_EXPIRED = -1;
    public static final int ERROR_401 = 100;
    public static final int ERROR_NETWORK = 101;
    public static final int ERROR_SERVER = 102;
    public static final int ERROR_UNKNOWN = 103;

    public int getErrorCode() {
        return errorCode;
    }

    public void setErrorCode(int errorCode) {
        this.errorCode = errorCode;
    }

    public String getErrorMessage() {
        return errorMessage;
    }

    public void setErrorMessage(String errorMessage) {
        this.errorMessage = errorMessage;
    }
}
