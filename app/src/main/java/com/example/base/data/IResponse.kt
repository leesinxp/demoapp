package com.example.base.data

interface IResponse<T> {
    /**
     * Will be called when the API call is totally successful in both HTTP and business logic
     *
     * @param data    Converted "data" object in the root JSON response
     */
    fun success(data: T)

    /**
     * Will be called when the API call is failed in either HTTP or business logic
     *
     * @param status  status code, UNKNOWN_ERROR for null response or connection error
     * @param message failure message:
     */
    fun failure(status: Int, message: String?)
}
