package com.example.base.data

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName
import java.io.Serializable


class Image : Serializable {
    @SerializedName("id")
    @Expose
     val id: Int? = null

    @SerializedName("type")
    @Expose
     val type: Any? = null

    @SerializedName("url")
    @Expose
     val url: String? = null

}