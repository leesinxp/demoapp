package com.example.base.data

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

class Data {
    @SerializedName("user")
    @Expose
    val user: User? = null

    @SerializedName("access_token")
    @Expose
    val accessToken: String? = null
}