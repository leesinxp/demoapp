package com.example.base.data

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName
import java.io.Serializable

class User: Serializable {
    @SerializedName("id")
    @Expose
    var id: Int? = null

    @SerializedName("name")
    @Expose
    var name: String? = null

    @SerializedName("email")
    @Expose
    var email: String? = null

    @SerializedName("facebook_id")
    @Expose
    var facebookId: Any? = null

    @SerializedName("id_number_location")
    @Expose
    val idNumberLocation: String? = null

    @SerializedName("id_number_date")
    @Expose
    val idNumberDate: String? = null

    @SerializedName("type")
    @Expose
    var type: Int? = null

    @SerializedName("image")
    @Expose
    var image: String? = null

    @SerializedName("phone")
    @Expose
    var phone: String? = null

    @SerializedName("birthday")
    @Expose
    var birthday: String? = null

    @SerializedName("address")
    @Expose
    var address: String? = null

    @SerializedName("province_id")
    @Expose
    var provinceId: String? = null

    @SerializedName("district_id")
    @Expose
    var districtId: String? = null

    @SerializedName("ward_id")
    @Expose
    var wardId: String? = null

    @SerializedName("gender")
    @Expose
    var gender: Int? = null

    @SerializedName("id_number")
    @Expose
    var idNumber: String? = null

    @SerializedName("temp_residence")
    @Expose
    var tempResidence: Any? = null

    @SerializedName("residence")
    @Expose
    var residence: Any? = null

    @SerializedName("folk")
    @Expose
    var folk: Any? = null

    @SerializedName("nation")
    @Expose
    var nation: Any? = null

    @SerializedName("religion")
    @Expose
    var religion: Any? = null

    @SerializedName("created_at")
    @Expose
    var createdAt: String? = null

    @SerializedName("updated_at")
    @Expose
    var updatedAt: String? = null

    @SerializedName("collect")
    @Expose
    var collect: Int? = null

    @SerializedName("spend")
    @Expose
    var spend: Int? = null

    @SerializedName("register_by")
    @Expose
    var registerBy: Int? = null

    @SerializedName("number_hostels")
    @Expose
    var numberHostels: Int? = null

    @SerializedName("number_rooms")
    @Expose
    var numberRooms: Int? = null

    @SerializedName("status")
    @Expose
    var status: Int? = null

    @SerializedName("current_room")
    @Expose
    var currentRoom: Any? = null

    @SerializedName("current_hostel")
    @Expose
    var currentHostel: Any? = null

    @SerializedName("join_date")
    @Expose
    var joinDate: Any? = null

    @SerializedName("code")
    @Expose
    var code: String? = null

    @SerializedName("is_owner_first")
    @Expose
    var isOwnerFirst: Int? = null

    @SerializedName("android_token")
    @Expose
    var androidToken: String? = null

    @SerializedName("ios_token")
    @Expose
    var iosToken: Any? = null

    @SerializedName("deleted_at")
    @Expose
    var deletedAt: Any? = null

    @SerializedName("status_email")
    @Expose
    var statusEmail: Int? = null

    @SerializedName("status_phone")
    @Expose
    var statusPhone: Int? = null

    @SerializedName("academic_level")
    @Expose
    var academicLevel: Any? = null

    @SerializedName("job")
    @Expose
    var job: Any? = null

    @SerializedName("job_address")
    @Expose
    var jobAddress: Any? = null

    @SerializedName("temp_residence_province")
    @Expose
    var tempResidenceProvince: Any? = null

    @SerializedName("temp_residence_district")
    @Expose
    var tempResidenceDistrict: Any? = null

    @SerializedName("temp_residence_ward")
    @Expose
    var tempResidenceWard: Any? = null

    @SerializedName("residence_province")
    @Expose
    var residenceProvince: Any? = null

    @SerializedName("residence_district")
    @Expose
    var residenceDistrict: Any? = null

    @SerializedName("residence_ward")
    @Expose
    var residenceWard: Any? = null

    @SerializedName("status_info")
    @Expose
    var statusInfo: Int? = null

    @SerializedName("first_name")
    @Expose
    var firstName: String? = null

    @SerializedName("last_name")
    @Expose
    var lastName: String? = null

    @SerializedName("first_add_renter")
    @Expose
    var firstAddRenter: Int? = null

    @SerializedName("send_notification_first")
    @Expose
    var sendNotificationFirst: Int? = null

    @SerializedName("date_send_notification")
    @Expose
    var dateSendNotification: Any? = null

    @SerializedName("is_connect_facebook")
    @Expose
    var isConnectFacebook: Int? = null

    @SerializedName("is_connect_google")
    @Expose
    var isConnectGoogle: Int? = null

    @SerializedName("google_id")
    @Expose
    var googleId: Any? = null

    @SerializedName("expire_date_owner")
    @Expose
    var expireDateOwner: Any? = null

    @SerializedName("is_fb_first")
    @Expose
    var isFbFirst: Int? = null

    @SerializedName("token_active")
    @Expose
    var tokenActive: String? = null

    @SerializedName("token_forget_password")
    @Expose
    var tokenForgetPassword: Any? = null

    @SerializedName("webpush_token")
    @Expose
    var webpushToken: Any? = null

    @SerializedName("from")
    @Expose
    var from: Int? = null

    @SerializedName("province_name")
    @Expose
    var provinceName: String? = null

    @SerializedName("district_name")
    @Expose
    var districtName: String? = null

    @SerializedName("ward_name")
    @Expose
    var wardName: String? = null

    @SerializedName("staff_owner_id")
    @Expose
    var staffOwnerId: Any? = null

    @SerializedName("is_over_date")
    @Expose
    var isOverDate: Int? = null

    @SerializedName("package_id")
    @Expose
    var packageId: Int? = null

    @SerializedName("id_number_front")
    @Expose
    var idNumberFront: String? = null

    @SerializedName("id_number_back")
    @Expose
    var idNumberBack: String? = null

    @SerializedName("date_joined")
    @Expose
    var dateJoined: String? = null

    @SerializedName("residence_status")
    @Expose
    var residenceStatus: Int? = null

    @SerializedName("date_end_residence")
    @Expose
    var dateEndResidence: String? = null

    @SerializedName("package")
    @Expose
    private var _package: Package? = null

    @SerializedName("refer_phone")
    @Expose
    var referPhone: Any? = null

    @SerializedName("refer_id")
    @Expose
    var referId: Any? = null

    @SerializedName("partner_company")
    @Expose
    var partnerCompany: Any? = null

    @SerializedName("partner_number_staff")
    @Expose
    var partnerNumberStaff: Int? = null

    @SerializedName("partner_refer")
    @Expose
    var partnerRefer: Any? = null

    @SerializedName("account_kit_code")
    @Expose
    var accountKitCode: Any? = null

    @SerializedName("day_remind_empty_room")
    @Expose
    var dayRemindEmptyRoom: Int? = null

    @SerializedName("allow_renter_make_group")
    @Expose
    var allowRenterMakeGroup: Int? = null

    @SerializedName("remind_empty_room_interval")
    @Expose
    var remindEmptyRoomInterval: Int? = null

    @SerializedName("allow_renter_view_hostel_group")
    @Expose
    var allowRenterViewHostelGroup: Int? = null

    @SerializedName("allow_renter_view_room_group")
    @Expose
    var allowRenterViewRoomGroup: Int? = null

    @SerializedName("domain")
    @Expose
    var domain: Any? = null

    @SerializedName("note")
    @Expose
    var note: String? = null

    @SerializedName("money_unit")
    @Expose
    var moneyUnit: String? = null

    @SerializedName("refer_code")
    @Expose
    var referCode: String? = null

    @SerializedName("room_id")
    @Expose
    var roomId: Int? = null

    @SerializedName("hostel_id")
    @Expose
    var hostelId: Int? = null

    @SerializedName("room_name")
    @Expose
    var roomName: String? = null

    @SerializedName("hostel_name")
    @Expose
    var hostelName: String? = null

    @SerializedName("hostel_type_rent")
    @Expose
    var hostelTypeRent: Int? = null

    @SerializedName("is_represent")
    @Expose
    var represent: Boolean? = null

    @SerializedName("contract_id")
    @Expose
    var contractId: Int? = null

    @SerializedName("bed_id")
    @Expose
    var bedId: Int? = null

    @SerializedName("bed_name")
    @Expose
    var bedName: String? = null

    @SerializedName("is_confirm_phone")
    @Expose
    val isConfirmPhone: Int? = null

    @SerializedName("is_confirm_id_number")
    @Expose
    val isConfirmIdNumber: Int? = null

    @SerializedName("province")
    @Expose
    val province: Location? = null

    @SerializedName("district")
    @Expose
    val district: Location? = null

    @SerializedName("ward")
    @Expose
    val ward: Location? = null


}