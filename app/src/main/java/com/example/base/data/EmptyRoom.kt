package com.example.base.data

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName
import java.io.Serializable


class EmptyRoom : Serializable {
    @SerializedName("id")
    @Expose
     val id: Int? = null

    @SerializedName("floor")
    @Expose
     val floor: Int? = null

    @SerializedName("size")
    @Expose
     val size: Int? = null

    @SerializedName("type")
    @Expose
     val type: Int? = null

    @SerializedName("price")
    @Expose
     val price: Int? = null

    @SerializedName("deposit")
    @Expose
     val deposit: Int? = null

    @SerializedName("debt")
    @Expose
     val debt: Int? = null

    @SerializedName("status")
    @Expose
     val status: Int? = null

    @SerializedName("current_electric")
    @Expose
     val currentElectric: Int? = null

    @SerializedName("current_water")
    @Expose
     val currentWater: Int? = null

    @SerializedName("current_renter")
    @Expose
     val currentRenter: Any? = null

    @SerializedName("hostel_id")
    @Expose
     val hostelId: Int? = null

    @SerializedName("desc")
    @Expose
     val desc: Any? = null

    @SerializedName("created_at")
    @Expose
     val createdAt: String? = null

    @SerializedName("updated_at")
    @Expose
     val updatedAt: String? = null

    @SerializedName("deleted_at")
    @Expose
     val deletedAt: Any? = null

    @SerializedName("name")
    @Expose
     val name: String? = null

    @SerializedName("start_electric")
    @Expose
     val startElectric: Any? = null

    @SerializedName("start_water")
    @Expose
     val startWater: Any? = null

    @SerializedName("date_available")
    @Expose
     val dateAvailable: Any? = null

    @SerializedName("max_renters")
    @Expose
     val maxRenters: Int? = null

    @SerializedName("long")
    @Expose
     val _long: Int? = null

    @SerializedName("width")
    @Expose
     val width: Int? = null

    @SerializedName("block_name")
    @Expose
     val blockName: Any? = null

    @SerializedName("is_empty")
    @Expose
     val isEmpty: Int? = null

    @SerializedName("lat")
    @Expose
     val lat: Any? = null

    @SerializedName("lng")
    @Expose
     val lng: Any? = null

    @SerializedName("is_display")
    @Expose
     val isDisplay: Int? = null

    @SerializedName("block_group")
    @Expose
     val blockGroup: String? = null

    @SerializedName("is_start_remind")
    @Expose
     val isStartRemind: Int? = null

    @SerializedName("cashback")
    @Expose
     val cashback: Int? = null

    @SerializedName("commission")
    @Expose
     val commission: Int? = null

    @SerializedName("images")
    @Expose
     val images: List<Image>? = null
}