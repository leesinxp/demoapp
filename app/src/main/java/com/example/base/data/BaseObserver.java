package com.example.base.data;

import android.util.Log;

import java.io.PrintWriter;
import java.io.StringWriter;
import java.net.HttpURLConnection;
import java.net.SocketTimeoutException;
import java.net.UnknownHostException;

import io.reactivex.observers.DisposableObserver;
import retrofit2.HttpException;

    public abstract class BaseObserver<T> extends DisposableObserver<BaseResponse<T>> implements IResponse<T> {

        public BaseObserver() {
        }

        @Override
        public void onError(Throwable e) {
            Log.d("BaseObserver", "onError: " + e.getMessage());
            if (e instanceof HttpException){
                if (((HttpException) e).code() == HttpURLConnection.HTTP_UNAUTHORIZED){
                    failure(BaseErrorSignal.ERROR_401, e.getMessage());
                }else {
                    failure(BaseErrorSignal.ERROR_SERVER, e.getMessage());
                }
            }else if (e instanceof UnknownHostException){
                failure(BaseErrorSignal.ERROR_NETWORK, e.getMessage());
            }else if (e instanceof SocketTimeoutException){
                failure(BaseErrorSignal.ERROR_NETWORK, e.getMessage());
            }else {
                StringWriter sw = new StringWriter();
                PrintWriter pw = new PrintWriter(sw);
                e.printStackTrace(pw);
                failure(BaseErrorSignal.ERROR_UNKNOWN, sw.toString());
            }
        }

        @Override
        public void onNext(BaseResponse<T> tResponse) {
            if (tResponse != null){
                // Has converted body
                if (tResponse.getStatus() > BaseErrorSignal.ERROR_HAS_MESSAGE){
                    // Success in business logic
                    success(tResponse.getData());
                }else if (tResponse.getStatus() == BaseErrorSignal.ERROR_HAS_MESSAGE ){
                    // Fail in business logic
                    failure(BaseErrorSignal.ERROR_HAS_MESSAGE, tResponse.getMessage());
                }else if (tResponse.getStatus() == BaseErrorSignal.ERROR_EXPIRED) {
                    failure(BaseErrorSignal.ERROR_EXPIRED, tResponse.getMessage());
                }else {
                    failure(BaseErrorSignal.ERROR_UNKNOWN, tResponse.getMessage());
                }
            }
        }

    }


