package com.example.base

import android.util.Log
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.example.base.data.BaseErrorSignal

open class BaseViewModel<T> : ViewModel(){
    val isLoading by lazy { MutableLiveData(false) }

    // error message
    val errorMessage by lazy { MutableLiveData<String>() }

    // error http message
    val errorHttpMessage by lazy { MutableLiveData<Int>() }


    var errorSignal: MutableLiveData<BaseErrorSignal> = MutableLiveData()

    fun resolveError(statusCode: Int, message: String?) {
        when (statusCode) {
            BaseErrorSignal.ERROR_HAS_MESSAGE -> { // has message
                errorSignal.postValue(BaseErrorSignal(BaseErrorSignal.ERROR_HAS_MESSAGE, message))
            }
            BaseErrorSignal.ERROR_401 -> {
                Log.e("ISG_Error", "401")
                errorSignal.postValue(BaseErrorSignal(BaseErrorSignal.ERROR_401, "Xin vui lòng đăng nhập lại"))
            }
            BaseErrorSignal.ERROR_SERVER -> {
                Log.e("ISG_Error", "server error $message")
                errorSignal.postValue(BaseErrorSignal(BaseErrorSignal.ERROR_SERVER, "Hệ thống đang được cập nhật. Vui lòng khởi động lại hoặc quay lại sau.."))
            }
            BaseErrorSignal.ERROR_NETWORK -> {
                hideLoading()
                Log.e("ISG_Error", "no network")
                errorSignal.postValue(BaseErrorSignal(BaseErrorSignal.ERROR_NETWORK, "Lỗi kết nối mạng. Kiểm tra lại đường truyền của bạn."))
            }
            BaseErrorSignal.ERROR_EXPIRED -> {
                Log.e("ISG_Error", "expired")
                errorSignal.postValue(BaseErrorSignal(BaseErrorSignal.ERROR_EXPIRED,"Phiên đăng nhập đã hết hạn, Vui lòng đăng nhập lại"))
            }
            BaseErrorSignal.ERROR_UNKNOWN -> {
                errorSignal.postValue(BaseErrorSignal(BaseErrorSignal.ERROR_UNKNOWN, "Có lỗi xảy ra. Liên lạc với nhà quản trị để được hỗ trợ."  ))
            }
            else -> {
            }
        }
    }

    fun showLoading() {
        isLoading.value = true
    }

    fun hideLoading() {
        isLoading.value = false
    }

}