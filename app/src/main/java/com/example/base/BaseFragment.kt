package com.example.base

import android.content.Context
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.annotation.LayoutRes
import androidx.databinding.DataBindingUtil
import androidx.databinding.DataBindingUtil.setContentView
import androidx.databinding.ViewDataBinding
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProviders
import com.example.base.data.BaseErrorSignal
import com.example.democallapi.BR
import dagger.android.support.AndroidSupportInjection
import dagger.android.support.DaggerFragment
import io.reactivex.disposables.CompositeDisposable


abstract class BaseFragment<VM : BaseViewModel<Any?>, VB : ViewDataBinding> : Fragment() {

     private lateinit var internalViewModel : VM
     private lateinit var internalViewBinding : VB

    val viewModel: VM
        get() = internalViewModel

    val viewBinding: VB
        get() = internalViewBinding


    @LayoutRes
    abstract fun layoutId(): Int
    abstract fun initView()

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        internalViewBinding =  DataBindingUtil.inflate(inflater, layoutId(), container, false)
        return internalViewBinding.root
    }
    abstract fun onCreateViewModel(): VM
    private val messageSubscription by lazy { CompositeDisposable() }
    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        internalViewModel = onCreateViewModel()
        internalViewBinding.apply {
            setVariable(BR.viewModel, internalViewModel)
            internalViewBinding.lifecycleOwner = viewLifecycleOwner
            root.isClickable = true
            executePendingBindings()
        }

        initView()


    }
    fun resolveError(error: BaseErrorSignal) {
        activity?.let {
            when (error.errorCode) {
                BaseErrorSignal.ERROR_HAS_MESSAGE -> {
                    handleErrorMessage(error.errorMessage)
                }
                BaseErrorSignal.ERROR_401 -> {
//                    refreshToken()
                }
                BaseErrorSignal.ERROR_NETWORK -> {
                    handleErrorMessage("Lỗi kết nối mạng. Kiểm tra lại đường truyền của bạn.")
                }
                BaseErrorSignal.ERROR_SERVER -> {
                    handleErrorMessage("Hệ thống đang được cập nhật. Vui lòng khởi động lại hoặc quay lại sau..")
                }
                BaseErrorSignal.ERROR_EXPIRED -> {
                    handleErrorMessage("Phiên đăng nhập đã hết hạn, Vui lòng đăng nhập lại")
                }
                BaseErrorSignal.ERROR_UNKNOWN -> {
                    handleErrorMessage("Có lỗi xảy ra. Liên lạc với nhà quản trị để được hỗ trợ.")
                }
                else -> {
                }
            }
        }

    }
    protected open fun handleErrorMessage(message: String?) {
        if (message.isNullOrBlank()) return
        requireActivity().showToast(message)
    }
}