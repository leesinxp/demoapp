package com.example.base

import android.os.Bundle
import androidx.annotation.LayoutRes
import androidx.appcompat.app.AppCompatActivity
import androidx.databinding.DataBindingUtil
import androidx.databinding.ViewDataBinding
import com.example.base.data.BaseErrorSignal
import com.example.democallapi.BR

abstract class BaseActivity< VM : BaseViewModel<Any?>, VB : ViewDataBinding> : AppCompatActivity(){
    private lateinit var internalViewBinding : VB
    private lateinit var internalViewModel: VM
    val viewModel: VM get() = internalViewModel
    val viewBinding : VB
         get() = internalViewBinding
    abstract fun onCreateViewModel(): VM

    @LayoutRes
    abstract fun layoutId(): Int
    abstract fun initView()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(layoutId())
        internalViewModel = onCreateViewModel()
        internalViewBinding = DataBindingUtil.setContentView(this, layoutId())
        internalViewBinding.apply {
            setVariable(BR.viewModel ,internalViewModel)
            internalViewBinding.lifecycleOwner = this@BaseActivity
            root.isClickable = true
            executePendingBindings()
        }
        initView()
    }
    fun resolveError(error: BaseErrorSignal) {
        this?.let {
            when (error.errorCode) {
                BaseErrorSignal.ERROR_HAS_MESSAGE -> {
                    handleErrorMessage(error.errorMessage)
                }
                BaseErrorSignal.ERROR_401 -> {
                    //refreshToken()
                }
                BaseErrorSignal.ERROR_NETWORK -> {
                    handleErrorMessage("Lỗi kết nối mạng. Kiểm tra lại đường truyền của bạn.")
                }
                BaseErrorSignal.ERROR_SERVER -> {
                    handleErrorMessage("Hệ thống đang được cập nhật. Vui lòng khởi động lại hoặc quay lại sau..")
                }
                BaseErrorSignal.ERROR_EXPIRED -> {
                    handleErrorMessage("Phiên đăng nhập đã hết hạn, Vui lòng đăng nhập lại")
                }
                BaseErrorSignal.ERROR_UNKNOWN -> {
                    handleErrorMessage("Có lỗi xảy ra. Liên lạc với nhà quản trị để được hỗ trợ.")
                }
                else -> {
                }
            }
        }
    }
    protected open fun handleErrorMessage(message: String?) {
        if (message.isNullOrBlank()) return
        showToast(message)
    }
}