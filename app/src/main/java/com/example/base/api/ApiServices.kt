package com.example.base.api

import androidx.databinding.BaseObservable
import androidx.databinding.Observable
import com.example.base.data.BaseResponse
import com.example.base.data.Data
import com.example.base.data.HomeCashBack
import retrofit2.http.Field
import retrofit2.http.GET
import retrofit2.http.POST
import retrofit2.http.Query

interface ApiServices {
    @POST("/login")
    fun login(
        @Field("phone") phone : String?,
        @Field("password") password : String?,
        @Field("firebase_token") firebaseToken : String?,
        @Field("token_type") token_type : String
    ) : io.reactivex.Observable<BaseResponse<Data>>
    @GET("hostel/list")
    fun getListHome(
       @Query("term") term: String?,
       @Query("limit") limit: Int?,
       @Query("offset") offset: Int,
       @Query("province_id") province_id: String?,
       @Query("district_id") district_id: String?,
       @Query("ward_id") ward_id: String?,
       @Query("sizes") sizes: String?,
       @Query("prices") prices: String?,
       @Query("type") type: Int?,
       @Query("is_empty") isEmpty: Int?
    ) : io.reactivex.Observable<BaseResponse<List<HomeCashBack>?>>
}