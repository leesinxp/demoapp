package com.example.base.api

import com.google.gson.GsonBuilder
import okhttp3.Interceptor
import okhttp3.OkHttpClient
import okhttp3.Response
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory

object Api {
    val apiServices : ApiServices by lazy {
        val logging = HttpLoggingInterceptor()
        logging.level = HttpLoggingInterceptor.Level.BODY
        val gson = GsonBuilder().serializeNulls().setPrettyPrinting().create()
        val client = OkHttpClient.Builder()
            .addInterceptor(logging)
//            .addInterceptor(HttpLoggingInterceptor)
            .build()
        Retrofit.Builder()
            .addConverterFactory(GsonConverterFactory.create(gson))
            .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
            .baseUrl(BASE_URL)
            .client(client)
            .build().create(ApiServices::class.java)
    }
    const val BASE_URL = "https://resident.vn/api/v1/"
//    class HttpHeaderInterceptor : Interceptor {
//        override fun intercept(chain: Interceptor.Chain): Response {
//            val request = chain.request().newBuilder()
//            if (UserManager.getAccessToken().isNotEmpty()) {
//                request.header("Authorization", "Bearer ${UserManager.getAccessToken()}")
//            }
//            return chain.proceed(request.build())
//        }
//    }
}