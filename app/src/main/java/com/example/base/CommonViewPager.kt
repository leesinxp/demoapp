package com.example.base

import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import androidx.fragment.app.FragmentStatePagerAdapter

class CommonViewPager(
    fm : FragmentManager,
    private val listFragment : ArrayList<Fragment>
) : FragmentStatePagerAdapter(fm, BEHAVIOR_RESUME_ONLY_CURRENT_FRAGMENT){
    override fun getCount(): Int = listFragment.size
    override fun getItem(position: Int): Fragment{
        return listFragment[position]
    }

}