package com.example.base

import android.content.Context
import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.example.account.AccountViewModel
import com.example.customer.CustomerViewModel
import com.example.democallapi.SignInViewModel
import com.example.group.GroupViewModel
import com.example.home.HomeViewModel
import com.example.main.MainResidentViewModel
import java.lang.IllegalStateException

@Suppress("UNCHECKED_CAST")
class ViewModelFactory(context: Context) : ViewModelProvider.Factory {
    override fun <T : ViewModel?> create(modelClass: Class<T>): T =
        with(modelClass){
            when{
                isAssignableFrom(SignInViewModel ::class.java)->{
                    SignInViewModel()
                }
                isAssignableFrom(MainResidentViewModel::class.java) ->{
                    MainResidentViewModel()
                }
                isAssignableFrom(HomeViewModel::class.java) ->{
                    HomeViewModel()
                }
                isAssignableFrom(CustomerViewModel::class.java) ->{
                    CustomerViewModel()
                }
                isAssignableFrom(GroupViewModel::class.java) ->{
                    GroupViewModel()
                }
                isAssignableFrom(AccountViewModel::class.java) ->{
                    AccountViewModel()
                }
                else -> throw IllegalStateException("unknow view model: $modelClass")
            }
        }as T

    companion object {
        fun getInstance(activity: Context): ViewModelFactory = ViewModelFactory(activity)
    }
}