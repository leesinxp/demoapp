package com.example.democallapi

import android.annotation.SuppressLint
import androidx.annotation.MainThread
import androidx.lifecycle.MutableLiveData
import com.example.base.BaseViewModel
import com.example.base.api.Api
import com.example.base.data.BaseObserver
import com.example.base.data.BaseResponse
import com.example.base.data.Data
import com.google.firebase.FirebaseApp
import com.google.firebase.iid.FirebaseInstanceId
import io.reactivex.Scheduler
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import io.reactivex.schedulers.Schedulers.io
import rx.schedulers.Schedulers.io

class SignInViewModel : BaseViewModel<Any?>() {
    var phone = MutableLiveData<String>()
    var password = MutableLiveData<String>()

    var signInResident = MutableLiveData<Data?>()

}