package com.example.democallapi

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.WindowManager
import androidx.lifecycle.Observer
import com.example.base.BaseActivity
import com.example.base.initViewModel
import com.example.democallapi.databinding.ActivityMainBinding

import com.example.main.MainResident
import com.google.android.material.bottomnavigation.BottomNavigationItemView
import com.google.android.material.bottomnavigation.BottomNavigationView
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : BaseActivity<SignInViewModel, ActivityMainBinding >() {
    override fun onCreateViewModel(): SignInViewModel = initViewModel()

    override fun layoutId(): Int = R.layout.activity_main

    override fun initView() {
        window.setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN)
        btn_log_in.setOnClickListener {
           startActivity(Intent(this, MainResident::class.java))
        }
    }

}