package com.example.common

import java.text.SimpleDateFormat
import java.util.*

object Constants {
    const val DEFAULT_FIRST_PAGE = 0
    const val DEFAULT_ITEM_PER_PAGE =20
    const val DEFAULT_NUM_VISIBLE_THRESHOLD = 5
    const val jDEFAULT_ITEM_PER_PAGE = 20
    const val THRESHOLD_CLICK_TIME = 250

    const val HOME = 0
    const val CUSTOMER = 1
    const val ACCOUNT = 3
    const val GROUP_FACEBOOK = 2
    const val BASE_URL = "https://resident.vn/api/v1/"
    const val BASE_URL_ITRO = "https://itro.vn"
    const val BASE_URL_RESIDENT = "https://resident.vn"

    const val DATA_USER = "data_user"
    const val TYPE_BANK = 0
    const val TYPE_ZALO_PAY = 1
    const val TYPE_MOMO = 2

    val sdf = SimpleDateFormat("yyyy-MM-dd HH:mm:ss")
    val sdfcustom = SimpleDateFormat("d/M/yyyy HH:mm")
    val myDateFormat = SimpleDateFormat("yyyy-MM-dd HH:mm", Locale.getDefault())

    val dateFormat = SimpleDateFormat("dd/MM/yyyy")
    val formatMonth = SimpleDateFormat("MM/yyyy")

    val dateFormatNoti = SimpleDateFormat("dd/MM/yyyy HH:mm:ss")

    val sdfdate = SimpleDateFormat("yyyy-MM-dd")
    val sdfdatecustom = SimpleDateFormat("dd/MM/yyyy")

    object Notification {
        const val TYPE_HOSTEL = "hostel"
        const val TYPE_VERIFY_PROFILE = "verify_profile"
        const val OBJECT_ID = "object_id"
        const val NOTIFICATION_ID = "notification_id"
        const val TYPE = "type"
        const val TYPE_CUSTOMER = "customer"
        const val TYPE_SCHEDULE = "schedule"
    }

    const val RC_CAMERA_PERM = 4466

    const val GENDER_MALE = 1
    const val GENDER_FEMALE = 0

    const val STATUS_QUAN_TAM = 0
    const val STATUS_HEN_XEM_NHA = 1
    const val STATUS_DA_XEM_NHA = 2
    const val STATUS_DA_DAT_COC = 3
    const val STATUS_HUY_COC = 4
    const val STATUS_DA_KY_HOP_DONG = 5
}