package com.example.group

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.example.base.BaseFragment
import com.example.base.initViewModel
import com.example.customer.CustomerViewModel
import com.example.democallapi.R
import com.example.democallapi.databinding.FragmentCustomerBinding
import com.example.democallapi.databinding.FragmentGroupBinding

class FragmentGroup : BaseFragment<GroupViewModel, FragmentGroupBinding>() {
    override fun onCreateViewModel(): GroupViewModel = initViewModel()

    override fun layoutId(): Int = R.layout.fragment_group

    override fun initView() {

    }

}