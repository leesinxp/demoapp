package com.example.binding

import android.view.View
import android.widget.ImageView
import android.widget.TextView
import androidx.databinding.BindingAdapter
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout
import com.bumptech.glide.Glide
import com.example.base.data.EmptyRoom
import com.example.base.data.HomeCashBack
import com.example.common.Constants
import com.google.android.material.textview.MaterialTextView

@BindingAdapter("goneLess")
fun goneLess(view : View, visibility : Boolean){
    if(visibility) view.visibility = View.VISIBLE else view.visibility = View.GONE
}
@BindingAdapter("titleToolBar")
fun setTitleToolBar(view : TextView, type : Int){
    when(type){
        Constants.HOME ->{
            view.text = "Trang chủ"
        }
        Constants.CUSTOMER ->{
            view.text = "Khách hàng"
        }
        Constants.GROUP_FACEBOOK ->{
            view.text = "Nhóm Facebook"
        }
        Constants.ACCOUNT ->{
            view.text = "Tài khoản"
        }
    }
}
@BindingAdapter("convertVnd")
fun convertVnd(view : MaterialTextView, home : HomeCashBack){
    if(home.maxPrice == home.minPrice){
        view.text = "Giá từ: " + String.format("%.1f", (home.minPrice)?.times(1.0)?.div(1000000)) + "tr"
    }else{
        view.text = "Giá từ: " + String.format("%.1f", (home.minPrice)?.times(1.0)?.div(1000000)) + "tr" + " - "+
                String.format("%.1f", (home.maxPrice)?.times(1.0)?.div(1000000)) + "tr"

    }
}
@BindingAdapter("shortMillionsVnd")
fun shortMillionsVnd(view : MaterialTextView, room : List<EmptyRoom>){
    if(room.size > 1){
        view.text = "${room[room.size - 1].cashback?.div(1000)}k - ${room[0].cashback?.div(1000)}k"
    }else{
        view.text = "${room[0].cashback?.div(1000)}k"
    }
}
@BindingAdapter("isRefreshLoading")
fun SwipeRefreshLayout.customRefreshing(refreshing : Boolean){
    isRefreshing = refreshing == true
}
@BindingAdapter("imgUrl")
fun ImageView.loadImage(imgUrl : String){
    Glide.with(this).load(imgUrl).into(this)
}