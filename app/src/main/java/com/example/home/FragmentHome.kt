package com.example.home

import androidx.lifecycle.Observer
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.example.base.BaseFragment
import com.example.base.data.HomeCashBack
import com.example.base.initViewModel
import com.example.binding.EndlessRecyclerViewScrollListener
import com.example.democallapi.R
import com.example.democallapi.databinding.FragmentHomeBinding
import kotlinx.android.synthetic.main.fragment_home.*

class FragmentHome : BaseFragment<HomeViewModel, FragmentHomeBinding>(){
    private val mListHome = ArrayList<HomeCashBack>()
    override fun onCreateViewModel(): HomeViewModel = initViewModel()

    override fun layoutId(): Int = R.layout.fragment_home

    override fun initView() {
         initRecyclerView()
        viewModel.listHome.observe(this, Observer {
            if (!it.isNullOrEmpty()) {
                mListHome.addAll(it)
                mHomeAdapter.submitList(mListHome)
            }
        })
        viewModel.listHomeRefresh.observe(this, Observer {
            mListHome.clear()
            if (!it.isNullOrEmpty()) {
                mListHome.addAll(it)
                mHomeAdapter.submitList(mListHome)
            }
        })
        viewBinding.refresh.setOnRefreshListener {
            viewModel.getListHome(true)
        }
        viewModel.errorSignal.observe(this, Observer {
            it?.let {
                resolveError(it)
            }
        })
    }
    private fun initRecyclerView(){
        viewBinding.rcvHome.apply {
            setHasFixedSize(true)
            adapter = mHomeAdapter
            layoutManager = LinearLayoutManager(context)
            val onScroll = object : EndlessRecyclerViewScrollListener(layoutManager as LinearLayoutManager){
                override fun onLoadMore(page: Int, totalItemsCount: Int, view: RecyclerView?) {

                        viewModel.getListHome(false)
                }

            }
            addOnScrollListener(onScroll as EndlessRecyclerViewScrollListener)
        }
    }
    private val mHomeAdapter by lazy {
        HomeCashBackAdapter()
    }

}