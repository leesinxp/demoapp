package com.example.home

import androidx.lifecycle.MutableLiveData
import com.example.base.BaseViewModel
import com.example.base.api.Api
import com.example.base.data.BaseObserver
import com.example.base.data.HomeCashBack
import com.example.common.Constants
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers

class HomeViewModel : BaseViewModel<Any?>() {
    val listHome = MutableLiveData<List<HomeCashBack>?>()
    val listHomeRefresh = MutableLiveData<List<HomeCashBack>?>()
    private var offset = 0
    init {
        getListHome(true)
    }
    fun getListHome(isRefresh : Boolean){
        if (isRefresh){
            offset
        }
        Api.apiServices.getListHome("", Constants.DEFAULT_ITEM_PER_PAGE, offset, null, null, null, null, null, null, 1)
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .doOnError { hideLoading() }
            .doOnNext { hideLoading() }
            ?.subscribe(object : BaseObserver<List<HomeCashBack>>(){
                override fun onComplete() {

                }

                override fun success(data: List<HomeCashBack>?) {
                    offset+= data?.size ?: 0
                    if (isRefresh) {
                        listHomeRefresh.value = data
                    }else{
                        listHome.value = data
                    }
                }

                override fun failure(status: Int, message: String?) {
                     resolveError(status, message)
                }

            })
    }
}