package com.example.home

import android.annotation.SuppressLint
import android.content.Intent
import android.os.Bundle
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView

import com.example.base.data.HomeCashBack
import com.example.democallapi.R
import com.example.democallapi.databinding.ItemListHomeBinding
import com.google.android.gms.common.util.DataUtils

class HomeCashBackAdapter : ListAdapter<HomeCashBack, ViewHolderCashBack>(DiffCallBackCashBack()){
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolderCashBack {
        val inflater = LayoutInflater.from(parent.context)
        val binding : ItemListHomeBinding = DataBindingUtil.inflate(inflater, R.layout.item_list_home,parent, false)
        return ViewHolderCashBack(
            binding
        )
    }

    override fun onBindViewHolder(holder: ViewHolderCashBack, position: Int) {
        holder.bindListHome(getItem(position))
    }

}
class ViewHolderCashBack(val binding : ItemListHomeBinding) : RecyclerView.ViewHolder(binding.root){
    fun bindListHome(home : HomeCashBack){
        itemView.run {
            binding.item = home
            binding.executePendingBindings()
           // setOnClickListener {
//                context.startActivity(Intent(context, HomeDetail::class.java).apply {
//                    val bundle = Bundle()
//                    home.id?.let { it1 -> bundle.putInt("data", it1) }
//                    bundle.putSerializable("putData", home)
//                    putExtras(bundle)
//                })
           // }

        }
    }
}
class DiffCallBackCashBack : DiffUtil.ItemCallback<HomeCashBack>(){
    override fun areItemsTheSame(oldItem: HomeCashBack, newItem: HomeCashBack): Boolean {
        return oldItem.id == newItem.id
    }

    @SuppressLint("DiffUtilEquals")
    override fun areContentsTheSame(oldItem: HomeCashBack, newItem: HomeCashBack): Boolean {
        return oldItem == newItem
    }

}