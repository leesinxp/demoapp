package com.example.account

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.example.base.BaseFragment
import com.example.base.initViewModel
import com.example.customer.CustomerViewModel
import com.example.democallapi.R
import com.example.democallapi.databinding.FragmentAccountBinding
import com.example.democallapi.databinding.FragmentGroupBinding

class FragmentAccount : BaseFragment<AccountViewModel, FragmentAccountBinding>() {
    override fun onCreateViewModel(): AccountViewModel = initViewModel()

    override fun layoutId(): Int = R.layout.fragment_account

    override fun initView() {

    }

}