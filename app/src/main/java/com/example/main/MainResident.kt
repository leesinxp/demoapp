package com.example.main

import android.graphics.Insets.add
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.WindowManager
import androidx.fragment.app.Fragment
import androidx.viewpager.widget.ViewPager
import com.example.account.FragmentAccount
import com.example.base.BaseActivity
import com.example.base.CommonViewPager
import com.example.base.initViewModel
import com.example.common.Constants
import com.example.customer.FragmentCustomer
import com.example.democallapi.R
import com.example.democallapi.databinding.ActivityMainResidentBinding
import com.example.group.FragmentGroup
import com.example.home.FragmentHome
import com.google.android.material.bottomnavigation.BottomNavigationView
import kotlinx.android.synthetic.main.activity_main_resident.*

class MainResident : BaseActivity<MainResidentViewModel, ActivityMainResidentBinding>() {

    var manager = supportFragmentManager
    override fun onCreateViewModel(): MainResidentViewModel = initViewModel()

    override fun layoutId(): Int = R.layout.activity_main_resident

    override fun initView() {
         window.setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN)
         viewBinding.bottomNavigation.setOnNavigationItemSelectedListener(bottomNavigationView)
         initBottom()
    }
    private val bottomNavigationView = BottomNavigationView.OnNavigationItemSelectedListener { item ->
        return@OnNavigationItemSelectedListener when(item.itemId){
            R.id.home_resident ->{
               viewModel.tab.value = Constants.HOME
               vp_main.currentItem = 0
                true
            }
            R.id.customer_resident ->{
                viewModel.tab.value = Constants.CUSTOMER
                vp_main.currentItem = 1
                true
            }
            R.id.group_resident ->{
                viewModel.tab.value = Constants.GROUP_FACEBOOK
                vp_main.currentItem = 2
                true
            }
            R.id.account_resident ->{
                viewModel.tab.value = Constants.ACCOUNT
                vp_main.currentItem = 3
                true
            }else ->false
        }
    }
    private fun initBottom(){
       viewBinding.vpMain.adapter = CommonViewPager(manager, ArrayList<Fragment>().apply {
           add(FragmentHome())
           add((FragmentCustomer()))
           add(FragmentGroup())
           add(FragmentAccount())
       })
        viewBinding.vpMain.addOnPageChangeListener(object : ViewPager.OnPageChangeListener{
            override fun onPageScrolled(position: Int, positionOffset: Float, positionOffsetPixels: Int) {

            }

            override fun onPageSelected(position: Int) {
                viewBinding.bottomNavigation.selectedItemId = when(position){
                    0 -> R.id.home_resident
                    1 -> R.id.customer_resident
                    2 -> R.id.group_resident
                    3 -> R.id.account_resident
                    else -> R.id.home_resident
                }
            }

            override fun onPageScrollStateChanged(state: Int) {

            }

        })
    }

}